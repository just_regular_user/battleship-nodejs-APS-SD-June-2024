
const Letters = require("./letters")
class Position {
    constructor(column, row) {
        if (!Letters[column]) {
            throw Error("Out of field")
        }
        if (row > 8) {
            throw new Error("Out of field")
        }
        this.column = column;
        this.row = row;
    }

    toString() {
        return this.column.toString() + this.row.toString()
    }

}

module.exports = Position;