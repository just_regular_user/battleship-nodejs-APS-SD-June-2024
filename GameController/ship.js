class Ship {
    constructor(name, size) {
        this.name = name;
        this.size = size;
        this.positions = [];
        this.hits = [];
    }
    addPosition(position) {
        this.positions.push(position);
    }

    isHit(shot) {
        let returnvalue = false
        this.positions.forEach(position => {
            if (position.row == shot.row && position.column == shot.column) // todo: check if already hit this point before
                returnvalue = true;
        });

        return returnvalue
    }

    registerHit(position) {
        this.hits.push(position);
    }

    isSunk() {
        return this.positions.every(position =>
            this.hits.some(hit => position.row == hit.row && position.column == hit.column)
        );
    }

    hasHits() {
        return this.hits.length > 0
    }
}

module.exports = Ship;