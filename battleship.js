const { Worker, isMainThread } = require('worker_threads');
const readline = require('readline-sync');
const gameController = require("./GameController/gameController.js");
const cliColor = require('cli-color');
const beep = require('beepbeep');
const position = require("./GameController/position.js");
const letters = require("./GameController/letters.js");
let telemetryWorker;

class Battleship {
    start() {
        telemetryWorker = new Worker("./TelemetryClient/telemetryClient.js");

        message("Starting...");
        telemetryWorker.postMessage({eventName: 'ApplicationStarted', properties:  {Technology: 'Node.js'}});

        printShip()
        this.InitializeGame();
        this.StartGame();
    }

    StartGame() {
        console.clear();
        printCannon()

        var turnNumber = 0;
        do {
            turnNumber++;
            console.log();
            message("╔─━━━━━━ ★ ━━━━━━─╗");
            message(`   Turn #${turnNumber}`);
            message("╚─━━━━━━ ★ ━━━━━━─╝");

            if (this.enemyFleet.some(s => s.isSunk())) message("Sunk ships: ")
            for (const ship of this.enemyFleet) {
                if (ship.isSunk()) {
                    message(`${ship.name}`)
                }
            }
            console.log();

            if (this.enemyFleet.some(s => !s.isSunk())) message("Alive ships: ")
            for (const ship of this.enemyFleet) {
                if (!ship.isSunk()) {
                    message(`${ship.name} is ${ship.hasHits() ? 'damaged' : 'whole'} `)
                }
            }
            console.log();

            message("Player, it's your turn");
            message("Enter coordinates for your shot :");
            var position = Battleship.GetPosition();
            var isHit = gameController.CheckIsHit(this.enemyFleet, position);

            telemetryWorker.postMessage({eventName: 'Player_ShootPosition', properties:  {Position: position.toString(), IsHit: isHit}});

            if (isHit) {
                beep();
                printDamage()

                if (isAllSunk(this.enemyFleet)) {
                    message("You win!")
                    process.exit()
                }
            } else {
                printMiss()
            }

            message(isHit ? "Yeah ! Nice hit !" : "Miss");

            var computerPos = this.GetEnemyStrikeRandomPosition();
            var isHit = gameController.CheckIsHit(this.myFleet, computerPos);

            telemetryWorker.postMessage({eventName: 'Computer_ShootPosition', properties:  {Position: computerPos.toString(), IsHit: isHit}});

            console.log();
            message(`Computer shot in ${computerPos.column}${computerPos.row} and ` + (isHit ? `has hit your ship !` : `miss`));
            if (isHit) {
                beep();
                printDamage()

                if (isAllSunk(this.myFleet)) {
                    message("You loose!")
                    process.exit()
                }
            }
        }
        while (true);

        // TODO add note for end game
    }

    static GetPosition() {
        while (true) {
            let input = readline.question()
            try {
                return Battleship.ParsePosition(input)
            } catch(_) {
                message("Wrong position, we have filed 8x8")
            }
        }
    }

    static ParsePosition(input) {
        var letter = letters.get(input.toUpperCase().substring(0, 1));
        var number = parseInt(input.substring(1, input.length), 10);
        return new position(letter, number);
    }

    GetRandomPosition() {
        var rows = 8;
        var lines = 8;
        var rndColumn = Math.floor((Math.random() * lines));
        var letter = letters.get(rndColumn + 1);
        var number = Math.floor((Math.random() * rows));
        var result = new position(letter, number);
        return result;
    }

    enemyPosition = {}
    GetEnemyStrikeRandomPosition() {
        do {
            var position = this.GetRandomPosition();
            if (!this.enemyPosition[position.toString()]) {
                this.enemyPosition[position.toString()] = position
                return position;
            }
        } while (true)
    }

    InitializeGame() {
        this.InitializeMyFleet();
        this.InitializeEnemyFleet();
    }

    InitializeMyFleet() {
        this.myFleet = gameController.InitializeShips();

        message("╔─━━━━━━ ★ ━━━━━━─╗");
        message("   Game Setup");
        message("╚─━━━━━━ ★ ━━━━━━─╝");
        message("Please position your fleet (Game board size is from A to H and 1 to 8) :");

        this.myFleet.forEach(function (ship) {
            console.log();
            message(`Please enter the positions for the ${ship.name} (size: ${ship.size})`);
            for (var i = 1; i < ship.size + 1; i++) {
                    message(`Enter position ${i} of ${ship.size} (i.e A3):`);
                    var position = Battleship.GetPosition()
                    telemetryWorker.postMessage({eventName: 'Player_PlaceShipPosition', properties:  {Position: position, Ship: ship.name, PositionInShip: i}});

                    ship.addPosition(position);
            }
        })
    }

    enemyFleets = []

    InitializeEnemyFleet() {
        var enemyFleet = gameController.InitializeShips();

        enemyFleet[0].addPosition(new position(letters.B, 4));
        enemyFleet[0].addPosition(new position(letters.B, 5));
        enemyFleet[0].addPosition(new position(letters.B, 6));
        enemyFleet[0].addPosition(new position(letters.B, 7));
        enemyFleet[0].addPosition(new position(letters.B, 8));

        enemyFleet[1].addPosition(new position(letters.E, 5));
        enemyFleet[1].addPosition(new position(letters.E, 6));
        enemyFleet[1].addPosition(new position(letters.E, 7));
        enemyFleet[1].addPosition(new position(letters.E, 8));

        enemyFleet[2].addPosition(new position(letters.A, 3));
        enemyFleet[2].addPosition(new position(letters.B, 3));
        enemyFleet[2].addPosition(new position(letters.C, 3));

        enemyFleet[3].addPosition(new position(letters.F, 8));
        enemyFleet[3].addPosition(new position(letters.G, 8));
        enemyFleet[3].addPosition(new position(letters.H, 8));

        enemyFleet[4].addPosition(new position(letters.C, 5));
        enemyFleet[4].addPosition(new position(letters.C, 6));

        this.enemyFleets.push(enemyFleet)

        enemyFleet = gameController.InitializeShips();

        enemyFleet[0].addPosition(new position(letters.A, 4));
        enemyFleet[0].addPosition(new position(letters.A, 5));
        enemyFleet[0].addPosition(new position(letters.A, 6));
        enemyFleet[0].addPosition(new position(letters.A, 7));
        enemyFleet[0].addPosition(new position(letters.A, 8));

        enemyFleet[1].addPosition(new position(letters.B, 5));
        enemyFleet[1].addPosition(new position(letters.B, 6));
        enemyFleet[1].addPosition(new position(letters.B, 7));
        enemyFleet[1].addPosition(new position(letters.B, 8));

        enemyFleet[2].addPosition(new position(letters.C, 3));
        enemyFleet[2].addPosition(new position(letters.C, 4));
        enemyFleet[2].addPosition(new position(letters.C, 5));

        enemyFleet[3].addPosition(new position(letters.D, 1));
        enemyFleet[3].addPosition(new position(letters.D, 2));
        enemyFleet[3].addPosition(new position(letters.D, 3));

        enemyFleet[4].addPosition(new position(letters.E, 5));
        enemyFleet[4].addPosition(new position(letters.E, 6));

        this.enemyFleets.push(enemyFleet)

        enemyFleet = gameController.InitializeShips();

        enemyFleet[0].addPosition(new position(letters.C, 4));
        enemyFleet[0].addPosition(new position(letters.C, 5));
        enemyFleet[0].addPosition(new position(letters.C, 6));
        enemyFleet[0].addPosition(new position(letters.C, 7));
        enemyFleet[0].addPosition(new position(letters.C, 8));

        enemyFleet[1].addPosition(new position(letters.A, 5));
        enemyFleet[1].addPosition(new position(letters.A, 6));
        enemyFleet[1].addPosition(new position(letters.A, 7));
        enemyFleet[1].addPosition(new position(letters.A, 8));

        enemyFleet[2].addPosition(new position(letters.F, 3));
        enemyFleet[2].addPosition(new position(letters.F, 4));
        enemyFleet[2].addPosition(new position(letters.F, 5));

        enemyFleet[3].addPosition(new position(letters.H, 1));
        enemyFleet[3].addPosition(new position(letters.H, 2));
        enemyFleet[3].addPosition(new position(letters.H, 3));

        enemyFleet[4].addPosition(new position(letters.B, 5));
        enemyFleet[4].addPosition(new position(letters.B, 6));

        this.enemyFleets.push(enemyFleet)

        enemyFleet = gameController.InitializeShips();

        enemyFleet[0].addPosition(new position(letters.B, 1));
        enemyFleet[0].addPosition(new position(letters.B, 2));
        enemyFleet[0].addPosition(new position(letters.B, 3));
        enemyFleet[0].addPosition(new position(letters.B, 4));
        enemyFleet[0].addPosition(new position(letters.B, 5));

        enemyFleet[1].addPosition(new position(letters.H, 1));
        enemyFleet[1].addPosition(new position(letters.H, 2));
        enemyFleet[1].addPosition(new position(letters.H, 3));
        enemyFleet[1].addPosition(new position(letters.H, 4));

        enemyFleet[2].addPosition(new position(letters.C, 3));
        enemyFleet[2].addPosition(new position(letters.D, 3));
        enemyFleet[2].addPosition(new position(letters.E, 3));

        enemyFleet[3].addPosition(new position(letters.C, 8));
        enemyFleet[3].addPosition(new position(letters.D, 8));
        enemyFleet[3].addPosition(new position(letters.E, 8));

        enemyFleet[4].addPosition(new position(letters.E, 4));
        enemyFleet[4].addPosition(new position(letters.F, 4));

        this.enemyFleets.push(enemyFleet)

        enemyFleet = gameController.InitializeShips();

        enemyFleet[0].addPosition(new position(letters.H, 1));
        enemyFleet[0].addPosition(new position(letters.H, 2));
        enemyFleet[0].addPosition(new position(letters.H, 3));
        enemyFleet[0].addPosition(new position(letters.H, 4));
        enemyFleet[0].addPosition(new position(letters.H, 5));

        enemyFleet[1].addPosition(new position(letters.F, 1));
        enemyFleet[1].addPosition(new position(letters.F, 2));
        enemyFleet[1].addPosition(new position(letters.F, 3));
        enemyFleet[1].addPosition(new position(letters.F, 4));

        enemyFleet[2].addPosition(new position(letters.C, 6));
        enemyFleet[2].addPosition(new position(letters.D, 6));
        enemyFleet[2].addPosition(new position(letters.E, 6));

        enemyFleet[3].addPosition(new position(letters.C, 1));
        enemyFleet[3].addPosition(new position(letters.D, 1));
        enemyFleet[3].addPosition(new position(letters.E, 1));

        enemyFleet[4].addPosition(new position(letters.E, 5));
        enemyFleet[4].addPosition(new position(letters.F, 5));

        this.enemyFleets.push(enemyFleet)

        this.enemyFleet = this.enemyFleets[Math.floor(Math.random() * this.enemyFleets.length)];

    }
}

module.exports = Battleship;

function printDamage() {
    console.log(cliColor.red("                \\         .  ./"));
    console.log(cliColor.red("              \\      .:\";'.:..\"   /"));
    console.log(cliColor.red("                  (M^^.^~~:.'\")."));
    console.log(cliColor.red("            -   (/  .    . . \\ \\)  -"));
    console.log(cliColor.red("               ((| :. ~ ^  :. .|))"));
    console.log(cliColor.red("            -   (\\- |  \\ /  |  /)  -"));
    console.log(cliColor.red("                 -\\  \\     /  /-"));
    console.log(cliColor.red("                   \\  \\   /  /"));
}

function printMiss() {
    console.log(cliColor.yellow("_\/_                 |                _\/_"))
    console.log(cliColor.yellow("/o\\             \       /            //o\\"))
    console.log(cliColor.yellow(" |                 .---.                |"))
    console.log(cliColor.yellow("_|_______     --  /     \  --     ______|__"))
    console.log(`"         \`${cliColor.blue("~^~^~^~^~^~^~^~^~^~^~^~")}\`"`)
}


function printCannon() {
    message("╔─━━━━━━ ★ ━━━━━━─╗");
    message("   Game Started");
    message("╚─━━━━━━ ★ ━━━━━━─╝");

    console.log(cliColor.black("                  __"));
    console.log(cliColor.black("                 /  \\"));
    console.log(cliColor.black("           .-.  |    |"));
    console.log(cliColor.black("   *    _.-'  \\  \\__/"));
    console.log(cliColor.black("    \\.-'       \\"));
    console.log(cliColor.black("   /          _/"));
    console.log(cliColor.black("  |      _  /"));
    console.log(cliColor.black("  |     /_\\'"));
    console.log(cliColor.black("   \\    \\_/"));
    console.log(cliColor.black("    \"\"\"\""));
}

function message(txt) {
    console.log(cliColor.yellow(txt))
}

function printShip() {

    console.log(cliColor.magenta("                                     |__"));
    console.log(cliColor.magenta("                                     |\\/"));
    console.log(cliColor.magenta("                                     ---"));
    console.log(cliColor.magenta("                                     / | ["));
    console.log(cliColor.magenta("                              !      | |||"));
    console.log(cliColor.magenta("                            _/|     _/|-++'"));
    console.log(cliColor.magenta("                        +  +--|    |--|--|_ |-"));
    console.log(cliColor.magenta("                     { /|__|  |/\\__|  |--- |||__/"));
    console.log(cliColor.magenta("                    +---------------___[}-_===_.'____                 /\\"));
    console.log(cliColor.magenta("                ____`-' ||___-{]_| _[}-  |     |_[___\\==--            \\/   _"));
    console.log(cliColor.magenta(" __..._____--==/___]_|__|_____________________________[___\\==--____,------' .7"));
    console.log(cliColor.magenta("|                        Welcome to Battleship                         BB-61/"));
    console.log(cliColor.magenta(" \\_________________________________________________________________________|"));
    console.log();
}

function isAllSunk(ships) {
    return ships.every(s => s.isSunk())
}